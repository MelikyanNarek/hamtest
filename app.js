const express = require('express');
const app = express();
const port = 5411;
const axios = require('axios');
const  bodyParser = require('body-parser');
// const User = require('./models/Users');
const { Telegraf } = require('telegraf');
const { message } = require('telegraf/filters');
const mongoose = require('mongoose'); // Ensure mongoose is imported
const https = require('https');
const fs = require('fs');
const path = require('path');
const cors = require('cors')

const options = {
    key: fs.readFileSync(path.resolve(__dirname, 'key.pem')),
    cert: fs.readFileSync(path.resolve(__dirname, 'cert.pem'))
};

// Define the User schema
const userSchema = new mongoose.Schema({
    _id: { type: mongoose.Schema.Types.ObjectId, required: true, auto: true },
    id: { type: Number, required: true },
    is_bot: { type: Boolean, required: false},
    first_name: { type: String, required: false },
    last_name: { type: String, required: false },
    username: { type: String, required: true },
    language_code: { type: String, required: true }
});
const Users = mongoose.model('Users', userSchema);

mongoose.connect('mongodb+srv://galstyan52:CJ3xJYfTiyI3n25A@clusterhk.qlu8tgh.mongodb.net/myDatabase?retryWrites=true&w=majority&appName=ClusterHK', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('Connected!'))
    .catch(err => console.error('Connection error', err));


const uuid = require('uuid');
const CryptoJS = require("crypto-js")

app.use(bodyParser.urlencoded({ extended: false, limit: '250mb' }));
app.use(bodyParser.json());
app.use(
    cors({
        origin: '*',
    })
);

const bot = new Telegraf('7265154333:AAH7w4c2OXz0t-Ukg2NmgOBT90Uw78BVVjQ')

bot.start(async (ctx) => {
    const userData = ctx.message.from;

    if (userData.is_bot === true) {
        return ctx.reply("You are bot!");
    }

    const dbUser = await Users.findOne({ id: userData.id })

    if (dbUser) {
        return ctx.reply("What is wrong!");
    }

    const newUser = new Users(userData);

    await newUser.save()

    ctx.reply("Hi there!", {
        reply_markup: {
            inline_keyboard: [
                /* Inline buttons. 2 side-by-side */
                [ { text: "Get guide video", callback_data: "btn-1" }/*, { text: "Button 2", callback_data: "btn-2" }*/ ],
            ]
        }
    })
});
bot.action('btn-1', (ctx) => {
    ctx.reply('Please wait');
    ctx.replyWithMediaGroup([
        {
            type: 'video',
            media: {
                source: './videoplayback.mp4',
                filename: 'videoplayback.mp4'
            }
        }
    ])
});
bot.launch()


app.post('/auth/hamster', async (req, res) => {
    try {
        const { init_data } = req.body;
        const isValidated = await verifyTelegramWebAppData(init_data);

        res.send(init_data);
        // if (!isValidated) {
        //     throw new Error('Not valid token');
        // }
        //
        // // update user
        // res.send("")
    } catch (e) {
        throw new Error(e);
    }

})
// app.listen(port, () => {
//     console.log(`Example app listening on port ${port}`)
// })
const server = https.createServer(options, app);

server.listen(443, () => {
    console.log('Server running at https://localhost:443/');
});

const verifyTelegramWebAppData = async (telegramInitData) => {
    const initData = new URLSearchParams(telegramInitData);
    console.log(initData);
    const hash = initData.get('hash');
    let dataToCheck  = [];

    initData.sort();
    initData.forEach((val, key) => key !== "hash" && dataToCheck.push(`${key}=${val}`));

    const secret = CryptoJS.HmacSHA256('7265154333:AAH7w4c2OXz0t-Ukg2NmgOBT90Uw78BVVjQ', "WebAppData");
    const _hash = CryptoJS.HmacSHA256(dataToCheck.join("\n"), secret).toString(CryptoJS.enc.Hex);

    return _hash === hash;
}

module.exports = {
    verifyTelegramWebAppData,
}